from troposphere import GetAtt, Output
from troposphere.iam import Policy, Role
from aws_base_notification import Notification
from aws_base_cloudtrail import Cloudtrail
from aws_base_config import Config
from aws_base_bucket import Buckets


class AwsBase(Notification, Cloudtrail, Config, Buckets):
    pass


t = AwsBase()

t.add_version()
t.add_description("aws base")
t.add_notification()  # TODO: Using KMS
t.add_cloudtrail()
# t.add_aws_config()  # TODO
# TODO: Add AWS Config Rules
# t.add_role()
t.add_bucket()
t.add_resource(Role(
    "LambdaExecutionRole",
    RoleName="lambda-execution-role",
    AssumeRolePolicyDocument={
        "Version": "2012-10-17",
        "Statement": [{
            "Effect": "Allow",
            "Principal": {
                "Service": ["lambda.amazonaws.com"]
            },
            "Action": ["sts:AssumeRole"]}]},
    Policies=[Policy(
        PolicyName="LambdaExecutionPolicy",
        PolicyDocument={
            "Version": "2012-10-17",
            "Statement": [{
                "Effect": "Allow",
                "Action": ["logs:*"],
                "Resource": "arn:aws:logs:*:*:*"}]
        }
    )]
))

t.add_output([
    Output(
        "LambdaExecutionRoleArn",
        Description="lambda execution role arn.",
        Value=GetAtt("LambdaExecutionRole", "Arn")
    )
])

print(t.to_json(sort_keys=False))
