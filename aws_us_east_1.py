import boto3
from troposphere import AWS_REGION, Equals, Ref, Parameter
# from troposphere.cloudtrail import Trail
# from troposphere.s3 import Bucket
# from troposphere.sns import Subscription, Topic, TopicPolicy
from aws_us_east_1_spending_alert import SpendingAlert
from aws_base_notification import Notification

cfn = boto3.client('cloudformation')
role_arn = cfn.describe_stacks(
    StackName='aws-base')['Stacks'][0]['Outputs'][0]['OutputValue']


class AwsBase(SpendingAlert, Notification):
    pass


t = AwsBase()

t.add_version()
t.add_description("aws base")
role_arn_param = t.add_parameter(Parameter(
    "RoleArn",
    Description="Arn of lambda excuting role.",
    Type="String",
    Default=role_arn
))
# TODO: move to condition class
t.add_condition("VerginiaOnly", Equals(Ref(AWS_REGION), "us-east-1"))
# TODO: t.add_resources.spending_alert()
t.add_notification(role_arn_param)
t.add_resources_spending_alert()

print(t.to_json(sort_keys=False))
