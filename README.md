# マネジメントコンソール事前手作業

### Billing

請求有効

### IAM

- root アカウントのMFA認証
- ユーザー作成
- `CreateAccountAlias`

# ローカル作業

### 開発環境

```shell
$ brew install jq
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirement.py
```

### スタック作成/更新

|ファイル名|スタック名|リージョン|説明| 補足 |
| :-- | :-- | :-- | :-- | :-- |
| aws_base.py | aws-base | ap-north-1 | Slack通知トピック<br>テンプレート用バケット | 基本 |
| aws_us_east_1.py | aws-us-east-1 | us-east-1 | 請求アラーム<br>Slack通知トピック | `aws-base` の Role に依存 |

```shell
$ ./cfn_run.sh aws_base.py
$ ./cfn_run.sh -r us-east-1 aws_us_east_1.py
```

# その他

### JSON 確認方法の例

```shell
$ python aws_base.py | jq '.' -C | less
```

### TODO

- モジュール化してスタックごとにディレクトリを分ける
- ローカルテスト
  - template のテスト
    - 使用するAPIテスト
    - validate
- CI 環境整備
- Slack Endpoint の KMS 暗号化（Slack通知トピックは base に依存する stack として切り出すし、ネストさせても良さそう）
  - `http://dev.classmethod.jp/cloud/aws/cloudtrail-with-kms-using-cfn/`
- aws-base 改良
  - プロジェクトごとの Cloudformation 管理バケット
  - IAM リードオンリーユーザー
- troposphere.Template < CommonTemplate(Condition, Vars定義) < CustomTemplate < StackTemplate にしたい

### プロジェクトごとの Cloudformation 管理バケットの構想

次のS3オブジェクト(JSONテンプレート)を配置するようにする
- `管理バケット/proj/region/stack.template` (マスターテンプレート)
- `管理バケット/proj/region/stack_test.template` (開発用テンプレート)

各プロジェクト
- 差分確認シェルを叩くとローカルテンプレートをS3上の開発用テンプレートにアップして差分を確認できる
- メインストリームブランチにマージされるとマスターテンプレートが更新される

CloudGateとLambdaでマスターテンプレートの未適用ステータスを返すAPIを用意し、
Datadogなどでデイリーで監視する。差分があればSlackにアラートが飛ぶ。
Lambdaは差分がなければマスターテンプレートを開発用テンプレートにマージする。
