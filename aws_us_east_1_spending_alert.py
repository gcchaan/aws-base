# Converted from S3_Bucket.template located at:
# http://aws.amazon.com/cloudformation/aws-cloudformation-templates/

from troposphere import GetAtt, Join, Parameter, Ref, Template
from troposphere.cloudwatch import Alarm, MetricDimension
from _aws_base_vars import NOTIFICATION

class SpendingAlert(Template):
    def add_resources_spending_alert(self): # TODO: arg actions=[""]
        self.add_resource(Alarm(
            "SpendingAlarm",
            # AlarmDescription=Join("", ["Alarm if AWS spending is over $", Ref("AlarmThreshold")]),
            AlarmDescription="Alarm if AWS spending is over $10",
            Namespace="AWS/Billing",
            MetricName="EstimatedCharges",
            Dimensions=[MetricDimension(Name="Currency", Value="USD")],
            Statistic="Maximum",
            Period=int(4 * 60 * 60), #second
            EvaluationPeriods=1,
            # Threshold=Ref("AlarmThreshold"),
            Threshold="10",
            ComparisonOperator="GreaterThanThreshold",
            OKActions=[Ref("SlackTopic")],
            # AlarmActions=[Ref(NOTIFICATION)],
            AlarmActions=[Ref("SlackTopic")],
            # InsufficientDataActions=[Ref(NOTIFICATION)],
            InsufficientDataActions=[Ref("SlackTopic")],
            Condition="VerginiaOnly",
            DependsOn="SlackTopic",
        ))
