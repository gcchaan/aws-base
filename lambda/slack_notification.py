import json
import re
from urllib.request import urlopen, Request

URL = 'https://hooks.slack.com/services/' \
      'T0LQJ6TL5/B57C7J94Y/7OVwPeImlCOZDfSfIBsxzMVQ'


class SlackPayload:
    def __init__(
            self,
            attachment={
                "title": 'TITLE',
                "title_link": '',
                "text": 'TEXT',
                "color": 'gray'},
            username='FROM_LAMBDA',
            icon_emoji=':frog:',
            channel='#proj_base_aws',
            link_names='1'):
        self.attachment = attachment
        self.username = username
        self.icon_emoji = icon_emoji
        self.channel = channel
        self.link_names = link_names
        self.__to_dict = {
            "attachments": [{
                "title": self.attachment['title'],
                "title_link": self.attachment['title_link'],
                "text": self.attachment['text'],
                "mrkdwn_in": ['text', 'pretext'],
                "color": self.attachment['color'],
            }],
            "username": self.username,
            "icon_emoji": self.icon_emoji,
            "channel": self.channel,
            "link_names": self.link_names,
        }

    @property
    def to_dict(self):
        return self.__to_dict


def build_watch_msg(message):
    if message['NewStateValue'] == 'OK':
        color = 'good'
    elif message['NewStateValue'] == 'ALARM':
        color = 'warning'
    else:
        color = 'gray'
    attachment = {
        "title": "[%s]%s" % (
            message['NewStateValue'],
            message['AlarmDescription']),
        "text": message['NewStateReason'],
        "color": color,
    }
    return SlackPayload(
        attachment,
        'AWS(Cloudwatch)',
        ':cloudwatch:',
        '#proj_base_aws',
        '1').to_dict


def build_cfn_msg(message):
    message_list = re.sub(r'$\n', '', message).replace('}\n', '}').split('\n')
    pre_messages = [kv.split('=') for kv in message_list]
    messages = {k: re.sub(r"^'|'$", '', v) for (k, v) in pre_messages}
    region = messages['StackId'].split(':')[3]
    title = "stack: %s(%s)" % (messages['StackName'], region)
    title_link = "https://%s.console.aws.amazon.com/cloudformation/home" \
        "?region=%s#/stack/detail?stackId=%s" % \
        (region, region, messages['StackId'])
    body = "`%s` is `%s`" % \
        (messages['ResourceType'], messages['ResourceStatus'])
    if messages['ResourceStatusReason']:
        body += " becase %s" % \
            (messages['ResourceStatusReason'])
    if re.search(r'FAILED$', messages['ResourceStatus']) is not None:
        color = 'danger'
        body += ' > @here'
    elif re.match('DELETE|ROLLBACK', messages['ResourceStatus']) \
            is not None:
        color = 'warning'
    elif re.search(r'COMPLETE$', messages['ResourceStatus']) is not None:
        color = 'good'
    else:
        color = 'gray'
    attachment = {
        "title": title,
        "title_link": title_link,
        "text": body,
        "color": color,
    }

    return SlackPayload(
        attachment,
        'AWS(CloudFormation)',
        ':cloudformation:').to_dict


def post(payload):
    req = Request(URL)
    params = json.dumps(payload).encode('utf-8')
    with urlopen(req, params) as res:
        data = res.read().decode('utf-8')
    return data


def lambda_handler(event, context):
    print('handler start')
    if not event:
        print('event clould not be found.')
        return

    print('Received event: ' + json.dumps(event, indent=2))
    subject = event['Records'][0]['Sns']['Subject']
    message = event['Records'][0]['Sns']['Message']
    if subject == 'AWS CloudFormation Notification':
        post_msg = build_cfn_msg(message)
    elif re.match('OK|ALARM|INSUFFICIENT', subject) is not None:
        post_msg = build_watch_msg(message)
    return post(post_msg)
