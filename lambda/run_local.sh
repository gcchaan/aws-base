#!/bin/bash

set -e

[[ -z $1 ]] && echo "usage: ./run_local PYTHON_FILE" && exit
readonly python_file=$1
readonly event_json=event_$(echo $python_file | sed s/.py//).json
! [[ -f "$python_file" ]] && echo "python file '$python_file' could not be found." && exit
! [[ -f "$event_json" ]] && echo "json file $event_json could not be found." && exit

flake8 "$python_file"
python-lambda-local -f lambda_handler "$python_file" "$event_json"
