from troposphere import GetAtt, Join, AWS_REGION
from troposphere import Parameter, Ref, Template
from troposphere.config import ConfigurationRecorder, DeliveryChannel
from troposphere.config import RecordingGroup
from troposphere.s3 import Bucket
from troposphere.iam import Policy, Role

class Config(Template):
    def add_aws_config(self):
        self.add_resource(Role(
            "AWSConfigRole",
            RoleName="aws-config-role",
            AssumeRolePolicyDocument={
                "Version": "2012-10-17",
                "Statement": [{
                    "Effect": "Allow",
                    "Principal": {
                        "Service": ["config.amazonaws.com"]
                    },
                    "Action": ["sts:AssumeRole"]}]},
            Policies=[Policy(
                PolicyName=Join("", ["AWSConfigDeliveryPermissions-", Ref(AWS_REGION)]),
                PolicyDocument={
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Action": ["config:Put*"],
                            "Resource": ["*"]
                        }, {
                            "Effect": "Allow",
                            "Action": ["s3:PutObject*"],
                            "Resource": [Join(
                                "",
                                [
                                    "arn:aws:s3:::",
                                    Ref("DeliveryChannelS3Bucket"),
                                    "/AWSLogs/*"
                                ]
                            ),]
                        }, {
                            "Effect": "Allow",
                            "Action": ["s3:GetBucketAcl"],
                            "Resource": [Join(
                                "",
                                [
                                    "arn:aws:s3:::",
                                    Ref("DeliveryChannelS3Bucket")
                                ]
                            )]
                        }
                    ]
                }
            )]
        ))

        config_bucket = self.add_resource(Bucket(
            "DeliveryChannelS3Bucket",
            DeletionPolicy="Retain"
        ))

        self.add_resource(ConfigurationRecorder(
            "ConfigurationRecorder",
            # Name=GetAtt("ConfigurationRecorderSanitisationResults", "ConfigurationRecorder"),
            RecordingGroup=RecordingGroup(
                AllSupported=True,
                IncludeGlobalResourceTypes=True
            ),
            RoleARN=GetAtt("AWSConfigRole", "Arn"),
            DependsOn="AWSConfigRole"
        ))

        self.add_resource(DeliveryChannel(
            "ConfigDeliveryChannel",
            S3BucketName=Ref(config_bucket),
            S3KeyPrefix="config",
            SnsTopicARN=Ref("SlackTopic"),
            DependsOn="AWSConfigRole"
        ))
