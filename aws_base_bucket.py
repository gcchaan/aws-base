from troposphere import Template
from troposphere.s3 import Bucket

class Buckets(Template):
    def add_bucket(self):
        self.add_resource(
            Bucket(
                "S3BucketForCloudformationTemplate",
                BucketName='aws-base-cloudformation',
            ))
        self.add_resource(
            Bucket(
                "S3BucketForLambda",
                BucketName='aws-base-lambda',
            ))
