from troposphere import GetAtt, Join, Parameter, Ref, Template
from troposphere.awslambda import Function, Code, Permission
from troposphere.logs import LogGroup, SubscriptionFilter
from troposphere.sns import Topic, Subscription
from _aws_base_vars import NOTIFICATION

def get_code():
    code_file = 'lambda/slack_notification.py'
    code = ["# Cloudformation managed - '%s'" % (code_file)]
    with open(code_file, 'r') as f:
        for row in f:
            code.append(row.replace('\n', ''))

    return code

class Notification(Template):
    def add_notification(self, role_arn_param=None):
        if not role_arn_param:
            lambda_excution_role_arn = GetAtt("LambdaExecutionRole", "Arn")
        else:
            lambda_excution_role_arn = Ref(role_arn_param)

        self.add_resource(Topic(
            "SlackTopic",
            Subscription=[Subscription(
                Endpoint=GetAtt("LambdaSlackNotification", "Arn"),
                Protocol="lambda"
            )],
            TopicName="SlackTopic"
        ))

        self.add_resource(Permission(
            "LambdaSlackNotificationPermission",
            FunctionName=GetAtt("LambdaSlackNotification", "Arn"),
            Action="lambda:InvokeFunction",
            Principal="sns.amazonaws.com",
            SourceArn=Ref("SlackTopic")
        ))

        self.add_resource(Function(
            "LambdaSlackNotification",
            Code=Code(ZipFile=Join("\n", get_code())),
            Handler="index.lambda_handler",
            Role=lambda_excution_role_arn,
            Runtime="python3.6",
            # KmsKeyArn="" , # TODO: Using KMS to encrypt SlackEndpoint.
            # MemorySize=Ref(MemorySize),
            Timeout=10,
        ))
