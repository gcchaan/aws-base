from troposphere import GetAtt, Join
from troposphere import Parameter, Ref, Template
from troposphere.s3 import Bucket
from troposphere.sns import Topic, Subscription
from troposphere.sns import TopicPolicy
from troposphere.s3 import BucketPolicy
from troposphere.cloudtrail import Trail
from troposphere.cloudwatch import Alarm
from troposphere.iam import Role, Policy
from troposphere.logs import LogGroup, MetricFilter, MetricTransformation

class Cloudtrail(Template):
    def add_cloudtrail(self):
        trail_bucket = self.add_resource(Bucket(
            "CloudTrailBucket",
            DeletionPolicy="Retain"
        ))

        self.add_resource(BucketPolicy(
            "BucketPolicy",
            PolicyDocument={
                "Version": "2012-10-17",
                "Statement": [{
                    "Action": "s3:GetBucketAcl",
                    "Principal": {
                        "Service": "cloudtrail.amazonaws.com"
                    },
                    "Resource": Join("", [
                        "arn:aws:s3:::", Ref(trail_bucket)
                    ]),
                    "Effect": "Allow",
                    "Sid": "AWSCloudTrailAclCheck"
                }, {
                    "Action": "s3:PutObject",
                    "Principal": {
                        "Service": "cloudtrail.amazonaws.com"
                    },
                    "Resource": Join("", [
                        "arn:aws:s3:::",
                        Ref(trail_bucket),
                        "/AWSLogs/",
                        Ref("AWS::AccountId"),
                        "/*"
                    ]),
                    "Effect": "Allow",
                    "Sid": "AWSCloudTrailWrite",
                    "Condition": {
                        "StringEquals": {"s3:x-amz-acl": "bucket-owner-full-control"}
                    }
                }]
            },
            Bucket=Ref(trail_bucket),
        ))

        self.add_resource(LogGroup(
            "CloudTrailLogGroup",
            LogGroupName="cloud-trail",
            RetentionInDays=90
            # 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.
        ))

        self.add_resource(Role(
            "CloudTrailLogRole",
            RoleName="cloudtrail-log-role",
            AssumeRolePolicyDocument={
                "Version": "2012-10-17",
                "Statement": [{
                    "Effect": "Allow",
                    "Principal": {
                        "Service": ["cloudtrail.amazonaws.com"]
                    },
                    "Action": ["sts:AssumeRole"]}]},
            Policies=[Policy(
                PolicyName="CloudTrailPolicy",
                PolicyDocument={
                    "Version": "2012-10-17",
                    "Statement": [{
                        "Effect": "Allow",
                        "Action": ["logs:*"],
                        "Resource": "arn:aws:logs:*:*:*"}]
                }
            )]
        ))

        self.add_resource(Trail(
            "myTrail",
            IsLogging=True,
            EnableLogFileValidation=True,
            IncludeGlobalServiceEvents=True,
            IsMultiRegionTrail=True,
            S3BucketName=Ref(trail_bucket),
            CloudWatchLogsLogGroupArn=GetAtt("CloudTrailLogGroup", "Arn"),
            CloudWatchLogsRoleArn=GetAtt("CloudTrailLogRole", "Arn"),
            DependsOn=["BucketPolicy"],
        ))

        # MetricFilter
        # https://www.slideshare.net/AmazonWebServicesJapan/aws-black-belt-online-seminar-2016-aws-cloudtrail-aws-config
        self.add_resource(MetricFilter(
            "CloudTrailChangesMetricFilter",
            LogGroupName=Ref("CloudTrailLogGroup"),
            FilterPattern="{ ($.eventName = CreateTrail) || ($.eventName = UpdateTrail) || ($.eventName = DeleteTrail) || ($.eventName = StartLogging) || ($.eventName = StopLogging) }",
            MetricTransformations=[MetricTransformation(
                MetricNamespace="CloudTrailMetrics",
                MetricName="CloudTrailEventCount",
                MetricValue="1"
            )]
        ))
        self.add_resource(Alarm(
            "CloudTrailChangesAlarm",
            AlarmName="CloudTrailChanges",
            AlarmDescription="Alarms when an API call is made to create, update or delete a CloudTrail trail, or to start or stop logging to a trail.",
            AlarmActions=[Ref("SlackTopic")],
            MetricName="CloudTrailEventCount",
            Namespace="CloudTrailMetrics",
            ComparisonOperator="GreaterThanOrEqualToThreshold",
            EvaluationPeriods=1,
            Period=300,
            Statistic="Sum",
            Threshold="1"
        ))


        self.add_resource(MetricFilter(
            "ConsoleSignInFailuresMetricFilter",
            LogGroupName=Ref("CloudTrailLogGroup"),
            FilterPattern="{ ($.eventName = ConsoleLogin) && ($.errorMessage = \"Failed authentication\") }",
            MetricTransformations=[MetricTransformation(
                MetricNamespace="CloudTrailMetrics",
                MetricName="ConsoleSignInFailureCount",
                MetricValue="1"
            )]
        ))
        self.add_resource(Alarm(
            "ConsoleSignInFailuresAlarm",
            AlarmName="CloudTrailConsoleSignInFailures",
            AlarmDescription="Alarms when an unauthenticated API call is made to sign into the console.",
            AlarmActions=[Ref("SlackTopic")],
            MetricName="ConsoleSignInFailureCount",
            Namespace="CloudTrailMetrics",
            ComparisonOperator="GreaterThanOrEqualToThreshold",
            EvaluationPeriods=1,
            Period=300,
            Statistic="Sum",
            Threshold="3"
        ))


        self.add_resource(MetricFilter(
            "AuthorizationFailuresMetricFilter",
            LogGroupName=Ref("CloudTrailLogGroup"),
            FilterPattern="{ ($.errorCode = \"*UnauthorizedOperation\") || ($.errorCode = \"AccessDenied*\") }",
            MetricTransformations=[MetricTransformation(
                MetricNamespace="CloudTrailMetrics",
                MetricName="AuthorizationFailureCount",
                MetricValue="1"
            )]
        ))
        self.add_resource(Alarm(
            "AuthorizationFailuresAlarm",
            AlarmName="CloudTrailAuthorizationFailures",
            AlarmDescription="Alarms when an unauthorized API call is made.",
            AlarmActions=[Ref("SlackTopic")],
            MetricName="AuthorizationFailureCount",
            Namespace="CloudTrailMetrics",
            ComparisonOperator="GreaterThanOrEqualToThreshold",
            EvaluationPeriods=1,
            Period=300,
            Statistic="Sum",
            Threshold="1"
        ))


        self.add_resource(MetricFilter(
            "IAMPolicyChangesMetricFilter",
            LogGroupName=Ref("CloudTrailLogGroup"),
            FilterPattern="{($.eventName=DeleteGroupPolicy)||($.eventName=DeleteRolePolicy)||($.eventName=DeleteUserPolicy)||($.eventName=PutGroupPolicy)||($.eventName=PutRolePolicy)||($.eventName=PutUserPolicy)||($.eventName=CreatePolicy)||($.eventName=DeletePolicy)||($.eventName=CreatePolicyVersion)||($.eventName=DeletePolicyVersion)||($.eventName=AttachRolePolicy)||($.eventName=DetachRolePolicy)||($.eventName=AttachUserPolicy)||($.eventName=DetachUserPolicy)||($.eventName=AttachGroupPolicy)||($.eventName=DetachGroupPolicy)}",
            MetricTransformations=[MetricTransformation(
                MetricNamespace="CloudTrailMetrics",
                MetricName="IAMPolicyEventCount",
                MetricValue="1"
            )]
        ))
        self.add_resource(Alarm(
            "IAMPolicyChangesAlarm",
            AlarmName="CloudTrailIAMPolicyChanges",
            AlarmDescription="Alarms when an API call is made to change an IAM policy.",
            AlarmActions=[Ref("SlackTopic")],
            MetricName="IAMPolicyEventCount",
            Namespace="CloudTrailMetrics",
            ComparisonOperator="GreaterThanOrEqualToThreshold",
            EvaluationPeriods=1,
            Period=300,
            Statistic="Sum",
            Threshold="1"
        ))
